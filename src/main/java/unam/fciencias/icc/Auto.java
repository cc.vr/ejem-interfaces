package unam.fciencias.icc;

public class Auto implements Vehículo {

  private int marcha;

  public Auto () {
    this.marcha = 0;
  }

  public int getMarcha() {
    return this.marcha;
  }

  public void subeVelocidad() {
    marcha = marcha + 1;
  }

  public void bajaVelocidad() {
    marcha = marcha - 1;
  }
  
  public void subeVelocidad(int velocidad) {
    marcha = marcha + velocidad;  
  }

  public void bajaVelocidad(int velocidad) {
    marcha = marcha - velocidad;
  }

  public double rendimiento(double distancia, double litros) {
    return distancia / litros;
  }

  public int distanciaRecorrida(int inicial, int terminal) {
    return terminal - inicial;
  }


}
