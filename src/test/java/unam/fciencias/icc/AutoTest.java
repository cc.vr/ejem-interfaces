package unam.fciencias.icc;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
 * Pruebas unitarias para la clase Auto.
 */
public class AutoTest {

   /**
    * El Auto enciende estando en marcha cero. OK
    */
   @Test
    public void elAutoEnciendeConMarchaCero() {
      Auto a = new Auto();
      assertTrue(a.getMarcha() == 0);   
    }
   
    /**
     * Cuando un Auto es encendido, al tratar de bajar
     * la marcha debe indicar una excepción de estado inválido. MODIFICAR
     */ 
    @Test
    public void elAutoNoPuedeBajarDeMarchaCuandoEnciende() {
      Auto a = new Auto();
      a.bajaVelocidad();
        assertTrue(a.getMarcha() == -1 );
    }

    /**
     * El Auto sube de marcha al encender. OK
     */
    @Test
    public void subeCambioEnUnoTest(){
      Auto a = new Auto();
      a.subeVelocidad();
      assertTrue(a.getMarcha() == 1);
    }

    /**
     * El Auto puede bajar de marcha siempre que tiene al menos una marcha válida. ESCRIBIR
     */

    /**
     * El Auto recorre una distancia que es positiva. MODIFICAR
     */ 
    @Test
    public void distanciaPositiva() {
      Auto a = new Auto();
      assertTrue(a.distanciaRecorrida(0, 9) == 9);
    }

    /**
     * Al encender el Auto tiene un rendimiento de cero KM/L. ESCRIBIR
     */

    /**
     * El Auto tiene un rendimiento de combustible positivo. ESCRIBIR
     */

}
